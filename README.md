# barcodetools


tools to get histogram of generated cell/synapse barcodes.

## install
```
$ mkdir build
$ cd build
$ cmake ..
$ make
./barcodetools sample_R1.fastq.gz whitelist.txt
```

## result
* barcodes.hist
* status.hist
* anchor.hist