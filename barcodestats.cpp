#include <iostream>
#include <fstream>
#include <htslib/kseq.h>
#include <htslib/bgzf.h>
#include <vector>
#include <algorithm>
#include <unordered_set>
#include <unordered_map>

const uint16_t MATCH_NONE = 0x001;
const uint16_t MATCH_EXACT = 0x010;
const uint16_t MATCH_1MM = 0x100;

// convert A->00, C->01, G->10, T->11
uint16_t convert_dna_to_ui16(const std::string& barcode) {

    if (barcode.length() > 8)
        return 0;

    uint16_t code = 0;
    for (char b : barcode) {
        switch (b) {
            case 'A': code = (code << 2) | 0; break;
            case 'C': code = (code << 2) | 1; break;
            case 'G': code = (code << 2) | 2; break;
            case 'T': code = (code << 2) | 3; break;
            default : code = (code << 2) | 0;
        }
    }
    return code;
}


std::string convert_ui16_to_dna(uint16_t code, int length = 8) {
    std::string barcode(length, 'A');
    if (length > 8)
        length = 8;
    for (int i = length - 1; i >= 0; --i) {
        switch (code & 3) {
            case 0: barcode[i] = 'A'; break;
            case 1: barcode[i] = 'C'; break;
            case 2: barcode[i] = 'G'; break;
            case 3: barcode[i] = 'T'; break;
        }
        code >>= 2;
    }
    return barcode;
}


uint16_t get_match(const std::string& qry_barcode, const std::unordered_set<uint16_t>& whitelist, uint16_t &status) {
    
    // exact match first
    uint16_t qry_code = convert_dna_to_ui16(qry_barcode);
    if (whitelist.find(qry_code) != whitelist.end()) {
        status = MATCH_EXACT;
        return qry_code;
    }

    // 1-base edit distance matches
    uint16_t mask, mutated;
    for (int i = 0; i < qry_barcode.length(); ++i) {
        for (int j = 0; j < 4; ++j) {
            mask = 3 << (i * 2);
            mutated = (qry_code & ~mask) | (j << (i * 2));
            if (whitelist.find(mutated) != whitelist.end()) {
                status = MATCH_1MM;
                return mutated;
            }
        }
    }

    // no match
    status = MATCH_NONE;
    return 0;
}


uint64_t pack_ui16_to_ui64(uint16_t bc1, uint16_t bc2 = 0, uint16_t bc3 = 0, uint16_t bc4 = 0) {
    uint64_t result = 0;
    result |= static_cast<uint64_t>(bc1);
    result <<= 16;
    result |= static_cast<uint64_t>(bc2);
    result <<= 16;
    result |= static_cast<uint64_t>(bc3);
    result <<= 16;
    result |= static_cast<uint64_t>(bc4);
    return result;
}


uint16_t unpack_ui64_to_ui16(uint64_t pack, int position) {
    if (position < 0 || position > 3) {
        return 0;
    }

    int shift = 16 * (3 - position);

    return static_cast<uint16_t>((pack >> shift) & 0xFFFF);
}


std::string code_to_status(uint16_t code) {
    std::string status = "NONE";
    switch (code) {
        case MATCH_NONE: status = "NONE"; break;
        case MATCH_EXACT: status = "EXACT"; break;
        case MATCH_1MM: status = "1MM"; break;
    }
    return status;
}


template <typename KeyType>
std::vector<std::pair<KeyType, int>> create_sorted_records(const std::unordered_map<KeyType, int>& counter_map) {
    std::vector<std::pair<KeyType, int>> sorted_records(counter_map.begin(), counter_map.end());
    std::sort(sorted_records.begin(), sorted_records.end(),
              [](const std::pair<KeyType, int>& record1, const std::pair<KeyType, int>& record2) {
                 return record1.second > record2.second; // reverse code
              });
    return sorted_records;
}


void print_barcodes(const std::vector<std::pair<uint64_t, int>>& sorted_records, const std::string& filename, int norm) {
    std::ofstream outputFile(filename);
    if (!outputFile.is_open()) {
        std::cerr << "Failed to open the file: " << filename << std::endl;
        return;
    }
    
    for (const auto& record : sorted_records) {
        outputFile << convert_ui16_to_dna(unpack_ui64_to_ui16(record.first, 0), 6) << '\t';
        outputFile << convert_ui16_to_dna(unpack_ui64_to_ui16(record.first, 1), 6) << '\t';
        outputFile << convert_ui16_to_dna(unpack_ui64_to_ui16(record.first, 2), 6) << '\t';
        outputFile << record.second << '\t';
        outputFile << std::fixed << std::setprecision(8) << (100.0 * record.second / norm)<< std::endl;
    }
    
    outputFile.close();
}


void print_status(const std::vector<std::pair<uint64_t, int>>& sorted_records, const std::string& filename, int norm) {
    std::ofstream outputFile(filename);
    if (!outputFile.is_open()) {
        std::cerr << "Failed to open the file: " << filename << std::endl;
        return;
    }
    
    for (const auto& record : sorted_records) {
        uint16_t status_a = unpack_ui64_to_ui16(record.first, 0);
        uint16_t status_b = unpack_ui64_to_ui16(record.first, 1);
        uint16_t status_c = unpack_ui64_to_ui16(record.first, 2);
        outputFile << code_to_status(status_a) << '\t';
        outputFile << code_to_status(status_b) << '\t';
        outputFile << code_to_status(status_c) << '\t';
        outputFile << record.second << '\t';
        outputFile << std::fixed << std::setprecision(8) << (100.0 * record.second / norm)<< std::endl;
    }
    
    outputFile.close();
}


void print_anchor(const std::vector<std::pair<uint16_t, int>>& sorted_records, const std::string& filename, int norm) {
    std::ofstream outputFile(filename);
    if (!outputFile.is_open()) {
        std::cerr << "Failed to open the file: " << filename << std::endl;
        return;
    }
    
    for (const auto& record : sorted_records) {
        outputFile << convert_ui16_to_dna(record.first, 8)<< '\t';
        outputFile << record.second << '\t';
        outputFile << std::fixed << std::setprecision(8) << (100.0 * record.second / norm)<< std::endl;
    }
    
    outputFile.close();
}


void print_mishit_barcodes(const std::vector<std::pair<std::string, int>>& sorted_records, const std::string& filename, int norm) {
    std::ofstream outputFile(filename);
    if (!outputFile.is_open()) {
        std::cerr << "Failed to open the file: " << filename << std::endl;
        return;
    }
    
    for (const auto& record : sorted_records) {
        outputFile << record.first << '\t';
        outputFile << record.second << '\t';
        outputFile << std::fixed << std::setprecision(8) << (100.0 * record.second / norm)<< std::endl;
    }
    
    outputFile.close();
}

KSEQ_INIT(BGZF*, bgzf_read)

int main(int argc, char* argv[]) {

    if (argc < 3) {
        std::cerr << "Usage: " << argv[0] << " <fastq_file>" << " <white_list>" << std::endl;
        return 1;
    }

    const std::string file_fq(argv[1]);
    const std::string file_bc(argv[2]);

    // read barcodes whitelist
    std::ifstream fs(file_bc);
    if (!fs.is_open()) {
        std::cerr << "Error opening barcode file: " << file_bc << std::endl;
        return 1;
    }

    std::unordered_set<uint16_t> whitelist;
    std::string barcode;

    while (std::getline(fs, barcode)) {
        whitelist.insert(convert_dna_to_ui16(barcode));
    }
    fs.close();
    
    // read fastq file
    BGZF* fp = bgzf_open(file_fq.c_str(), "r");
    if (!fp) {
        std::cerr << "Error opening file: " << file_fq << std::endl;
        return 1;
    }

    std::unordered_map<std::string, int> counter_none_a;
    std::unordered_map<std::string, int> counter_none_b;
    std::unordered_map<std::string, int> counter_none_c;

    std::unordered_map<uint64_t, int> counter_barcodes;
    std::unordered_map<uint64_t, int> counter_status;
    std::unordered_map<uint16_t, int> counter_anchor;
    kseq_t* fqrecord = kseq_init(fp);
    int count = 0;
    int count_mishit_a = 0;
    int count_mishit_b = 0;
    int count_mishit_c = 0;
    while (kseq_read(fqrecord) >= 0) {
        std::string name(fqrecord->name.s);
        std::string seq(fqrecord->seq.s);
        std::string qual(fqrecord->qual.s);

        // barcode template XXXXXXGAXXXXXXGTXXXXXXTTTT
        // posiotions 0, 8, 16
        std::string barcode_a = seq.substr(0, 6);
        std::string barcode_b = seq.substr(8, 6);
        std::string barcode_c = seq.substr(16, 6);
        std::string anchor = seq.substr(6, 2) + seq.substr(14, 2) + seq.substr(22, 4);

        // pack code
        uint16_t code_anchor = convert_dna_to_ui16(anchor);
        uint16_t status_a, status_b, status_c = MATCH_NONE;
        uint16_t code_a = get_match(barcode_a, whitelist, status_a);
        uint16_t code_b = get_match(barcode_b, whitelist, status_b);
        uint16_t code_c = get_match(barcode_c, whitelist, status_c);
        uint64_t key_code = pack_ui16_to_ui64(code_a, code_b, code_c);
        uint64_t key_status = pack_ui16_to_ui64(status_a, status_b, status_c);

        // increment maps
        ++counter_barcodes[key_code];
        ++counter_status[key_status];
        ++counter_anchor[code_anchor];
        if (status_a == MATCH_NONE) {
            ++counter_none_a[barcode_a];
            count_mishit_a++;
        }
            

        if (status_b == MATCH_NONE) {
            ++counter_none_b[barcode_b];
            count_mishit_b++;
        }
            

        if (status_c == MATCH_NONE) {
            ++counter_none_c[barcode_c];
            count_mishit_c++;
        }
            

        count++;
        // if (count == 1000000)
        //     break;
    }
    kseq_destroy(fqrecord);
    bgzf_close(fp);

    std::vector<std::pair<uint64_t, int>> sorted_counter_barcodes = create_sorted_records(counter_barcodes);
    std::vector<std::pair<uint64_t, int>> sorted_counter_status = create_sorted_records(counter_status);
    std::vector<std::pair<uint16_t, int>> sorted_counter_anchor = create_sorted_records(counter_anchor);
    std::vector<std::pair<std::string, int>> sorted_counter_none_a = create_sorted_records(counter_none_a);
    std::vector<std::pair<std::string, int>> sorted_counter_none_b = create_sorted_records(counter_none_b);
    std::vector<std::pair<std::string, int>> sorted_counter_none_c = create_sorted_records(counter_none_c);



    print_barcodes(sorted_counter_barcodes, "barcodes.hist", count);
    print_status(sorted_counter_status, "status.hist", count);
    print_anchor(sorted_counter_anchor, "anchor.hist", count);
    print_mishit_barcodes(sorted_counter_none_a, "mishit_a.hist", count_mishit_a);
    print_mishit_barcodes(sorted_counter_none_b, "mishit_b.hist", count_mishit_b);
    print_mishit_barcodes(sorted_counter_none_c, "mishit_c.hist", count_mishit_c);

    return 0;
}
